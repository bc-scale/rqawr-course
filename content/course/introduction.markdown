+++
title = "Introduction to the course, reproducibilty, and open science"

date = 2019-03-04T00:00:00

draft = false  # Is this a draft? true/false
toc = true  # Show table of contents? true/false
type = "docs"  # Do not modify.

linktitle = "Introduction to course"
[menu.course]
  parent = "Course"
  url = "/slides/introduction.html"
  weight = 5
+++

