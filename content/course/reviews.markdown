+++
title = "Reviews of previous sessions"

date = 2019-02-07T00:00:00

draft = false  # Is this a draft? true/false
toc = true  # Show table of contents? true/false
type = "docs"  # Do not modify.

linktitle = "Reviews"
[menu.course]
  parent = "Course"
  weight = 900
+++

**Slides**:

- [Review of Day 1](/slides/review-day-1.html)
- [Review of Day 2](/slides/review-day-2.html)
- [Review of Day 3](/slides/review-day-3.html)
